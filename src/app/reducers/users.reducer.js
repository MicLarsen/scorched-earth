export default function() {
    return [
        {
            id: 1,
            firstName: "Michael",
            lastName: "Rulle",
            login: "rulle"
        },
        {
            id: 2,
            firstName: "Michael",
            lastName: "Larsen",
            login: "larsen"
        }
    ]
}