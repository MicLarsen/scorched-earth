import { combineReducers } from 'redux'
import userReducer from './users.reducer'

const reducers = combineReducers({
    users: userReducer,
})

export default reducers
