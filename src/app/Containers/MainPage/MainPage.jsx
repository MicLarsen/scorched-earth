import React, { Component } from 'react'

class MainPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            buttonPressed: false,
            canvasHeight: 0,
            canvasWidth: 0,
            loggedIn: false,
            username: 'not logged in',
        }
    }

  
    componentDidMount() {
        this.updateCanvas(this.state.size)
    }

    componentDidUpdate() {
        this.updateCanvas(this.state.size)
    }
    
    updateCanvas(size) {
        let ctx = this.refs.canvas.getContext('2d')
        ctx.fillRect(0,0,100,100)      
    }

    showCanvas = () => {
        this.setState({buttonPressed: !this.state.buttonPressed})
        this.state.buttonPressed ? this.setState({canvasHeight: 0, canvasWidth: 0}) : this.setState({canvasHeight: 100, canvasWidth: 100})
    }

    log = () => {
        this.setState({ loggedIn: !this.state.loggedIn })
        if(!this.state.loggedIn) {
            this.setState({ username: 'not logged in' })
        } else {
            this.setState({ username: 'Michael'})
        }
    }

    render() {
        return(
            <div>
                <div>
                    MAIN!!! min opløsning 1024 x 768 - canvas min size 800 x 600
                </div>
                <div>
                    Logged in as: { this.state.username }
                </div>
                <div>
                    <button style={{height: 100, width: 200}} onClick={this.log}>
                        { this.state.loggedIn ? 'Login' : 'Logout' }
                    </button>
                </div>            
                <div>
                    <button style={{height: 200, width: 200}} onClick={this.showCanvas}>
                        Show Canvas ? {' ' + this.state.buttonPressed.toString()} 
                    </button>
                </div>
                <div>
                    <canvas ref="canvas" height={this.state.canvasHeight} width={this.state.canvasWidth}/>
                </div>
            </div>
        )
    }    
}

export default MainPage