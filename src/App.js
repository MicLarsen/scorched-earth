import React, { Component } from 'react';
import { Provider } from 'react-redux'

import store from './app/store/store'
import MainPage from './app/Containers/MainPage/MainPage'

import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Scorched Earth Anno 2018</h1>
        </header>
        <MainPage />
      </div>
      </Provider>
    );
  }
}

export default App;
